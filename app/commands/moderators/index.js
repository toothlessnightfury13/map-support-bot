const unMAP = require('./unMAP')
const cleanup = require('./cleanup')

module.exports = {
  commands: { 'Moderators Commands': { unMAP, cleanup } },
}
