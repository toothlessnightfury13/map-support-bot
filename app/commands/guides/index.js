const addMAP = require('./addMAP')
const onboard = require('./onboard')
const userInfo = require('./userInfo')

module.exports = {
  commands: { 'Guide Commands': { addMAP, onboard, userInfo } },
}
