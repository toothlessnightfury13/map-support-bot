const addAWA = require('./addAWA')
const addMinor = require('./addMinor')
const deleteMinor = require('./deleteMinor')
const getDMs = require('./getDMs')
const addRole = require('./addRole')

module.exports = {
  commands: { 'Administrator Commands': { addAWA, addRole } },
}
