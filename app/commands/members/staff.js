const mention = require('./mention')

async function staff({ bot, message, context }) {
  await mention.call({ bot, message, context })
}

module.exports = {
  description: `Alias for '${process.env.ROCKETCHAT_PREFIX} mention'. ${mention.description}`,
  help: mention.help,
  call: staff,
  cooldown: mention.cooldown,
}
