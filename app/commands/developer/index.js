const roomInfo = require('./roomInfo')
const testCommand = require('./testCommand')
const testCooldown = require('./testCooldown')

module.exports = {
  commands: { 'Developer Commands': { roomInfo, testCommand, testCooldown } },
}
