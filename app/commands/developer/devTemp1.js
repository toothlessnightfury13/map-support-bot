const getUserByUsername = require('../../utils/getUserByUsername')

const getUsersList = require('../../utils/getUsersList')
const getUserInfo = require('../../utils/getUserInfo')
const minors = require('../../../secret/minors-lowercase.json')

const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')

const SHOULD_ADD_USERS = true

async function getAllUsers(offset = 0) {
  let count = 100

  let result = await getUsersList({
    fields: {
      name: 1,
      username: 1,
      emails: 1,
    },
    query: {
      type: { $in: ['user'] },
    },
    count,
    offset,
  })

  let members = result.users

  if (members.length === count) {
    return [...members, ...(await getAllUsers(offset + count))]
  } else {
    return members
  }
}

async function devTemp1({ bot, message, context }) {
  const roomID = message.rid

  let members = await getAllUsers()

  await bot.sendToRoom(`Checking ${members.length} members in total...`, roomID)

  let progress = 0
  let minorsCount = 0
  for (let member of members) {
    let isMinor = false
    progress += 1

    // Give progress reports
    if (progress === 100) {
      progress = 0
      await bot.sendToRoom(`Am still checking...`, roomID)
    }

    for (let email of member.emails) {
      let address = email.address

      // Check emails
      if (minors.emails.includes(address.toLowerCase())) {
        isMinor = true
        minorsCount = minorsCount + 1
      }
    }

    if (!isMinor) {
      // Check username
      if (minors.usernames.includes(member.username.toLowerCase())) {
        console.log(member.username, minors.usernames.includes(member.username))
        isMinor = true
        minorsCount = minorsCount + 1
      }
    }

    if (SHOULD_ADD_USERS) {
      let targetRole = ''
      if (isMinor) {
        targetRole = 'minor'
      } else if (!isMinor) {
        targetRole = 'DM'
      }

      // Check if the member does not already have the role
      if (!userHasRole(member, targetRole)) {
        // Add the role to member
        await addUserToRole(member, targetRole)
      }
    }
  }

  await bot.sendToRoom(
    `Found ${minorsCount} minors out of ${members.length} members :hansen:`,
    roomID,
  )
}

module.exports = {
  description: 'Development Script',
  help: `${process.env.ROCKETCHAT_PREFIX}`,
  requireOneOfRoles: ['admin'],
  call: devTemp1,
}
