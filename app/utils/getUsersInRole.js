const { api } = require('@rocket.chat/sdk')

const getUsersInRole = async (role, roomId) => {
  let result = await api.get('roles.getUsersInRole', { role, roomId, count: 0 })
  if (result.success) {
    return result
  } else {
    console.error(result)
  }
}

module.exports = getUsersInRole
