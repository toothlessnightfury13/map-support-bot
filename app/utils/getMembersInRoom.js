const { api } = require('@rocket.chat/sdk')

const getMembersInRoom = async (object) => {
  let result = await api.get('channels.members', object)
  if (result.success) {
    return result
  } else {
    console.error(result)
  }
}

module.exports = getMembersInRoom
