const { api } = require('@rocket.chat/sdk')

const getDirectMessages = async (options = undefined, options2 = undefined) => {
  let allDMs = await api.get('im.list.everyone', options)
  let result = await api.get('im.messages.others', {
    roomId: allDMs.ims[0]._id,
    ...options2,
  })
  if (result.success) {
    return result
  } else {
    console.error(result)
  }
}

module.exports = getDirectMessages
