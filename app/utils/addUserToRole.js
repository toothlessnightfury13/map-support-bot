const { api } = require('@rocket.chat/sdk')

const addUserToRole = async (user, roleName, roomId = undefined) => {
  let result = await api.post('roles.addUserToRole', {
    roleName,
    username: user.username,
    roomId,
  })
  if (result.success) {
    return result.role
  } else {
    console.error(result)
  }
}

module.exports = addUserToRole
