const { api } = require('@rocket.chat/sdk')

const inviteUserToChannel = require('./inviteUserToChannel')

const inviteUserToChannels = async (user, channels) => {
  let invites = []
  for (let channel of channels) {
    invites.push(await inviteUserToChannel(user, channel))
  }
  return invites
}

module.exports = inviteUserToChannels
