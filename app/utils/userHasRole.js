const userHasRole = (user, role = []) => {
  if (user.roles.includes(role)) {
    return true
  } else {
    return false
  }
}

module.exports = userHasRole
